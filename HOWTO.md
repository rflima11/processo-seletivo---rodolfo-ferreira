# Requisitos para execução do projeto.

1. O projeto foi feito utilizando TOMCAT versão 9.0.30 com a API rodando na **porta padrão 8080** então ela deve ser utilizada para rodar a API com base nas configurações feitas pelo axios para a aplicação funcionar.

2. Foi utilizado vueCLi versão 4.4.1 

3. Para rodar a aplicação em modo desenvolvimento acessar a pasta "WebContent" e executar o comando:

``npm run dev``

Porém, foram feitos testes em outras máquinas e o comando acima tem apresentado problema, caso isso aconteça favor rodar os seguintes comando na pasta "WebContent"

`npm uninstall webpack-dev-server`

`npm install webpack-dev-server@2.9.1 --dev`

`npm run dev`

4. Para que o Hibernate construa as tabelas é necessário um database criado no MySQL Server nomeado como "mercado"

5. A senha do database foi alterado de **root** para **12345**


6. Para execução dos testes de requisições foi utilizado o programa "Postman"

Apesar da requisição PUT no código alterar o produto, infelizmente a opção de atualizar o produto não foi incluida com o axios na página, não tive tempo. Estou a disposição para qualquer dúvida em relação ao código, agradeço a oportunidade de participar do processo seletivo, aprendi bastante e no final é isso que vale. Obrigado!



