import { http } from './config'

export default {

    listar:() => {
        return http.get('fabricantes')
    },

    salvar:(fabricante) => {
        return http.post('fabricantes/add', fabricante)
    },

    apagar:(fabricante) => {
        return http.delete('fabricantes/delete/' +  fabricante.id )
    }

}