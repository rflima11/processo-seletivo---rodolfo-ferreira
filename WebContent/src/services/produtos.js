import { http } from './config'

export default {

    listar:() => {
        return http.get('produtos')
    },

    salvar:(produto) => {
        return http.post('produtos/add', produto)
    },

    apagar:(produto) => {
        return http.delete('produtos/delete/' +  produto.id )
    },

    atualizar:(produto) => {
        return http.put('produto/update/' + produto.id, produto)
    }

}